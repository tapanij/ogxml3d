var OG = {};

OG.Orientations = {
	zPlus: 0,
	zMinus: 1,
	yPlus: 2,
	yMinus: 3,
	xMinus: 4,
	xPlus: 5
}

OG.Alignments = {
	LeftBottom: 0,
	RightBottom: 1,
	LeftUpper: 2,
	RightUpper: 3
}

OG.Vector3 = function(x, y, z) {
	this.x = x || 0;
	this.y = y || 0;
	this.z = z || 0;
}

OG.updateVertex = function() {
	// var tf = element.getAttribute("transform");
	if (selectedElement != null) {
		for (i = 0; i < selectedElement.getAttribute("vertexCount"); i++) {
			// console.log("element clicked");
			// console.log($("#position").text());
			var vertices = $("#position").text();
			// console.log(vertices);

			// console.log(vertices.charAt(index*2));
			var index = selectedElement.getAttribute("index" + (i + 1));
			console.log("index " + index);

			var xSlider = parseInt($("#sliderX").text());
			var ySlider = parseInt($("#sliderY").text());
			var zSlider = parseInt($("#sliderZ").text());
			console.log("xSlider " + xSlider);
			console.log("ySlider " + ySlider);
			console.log("zSlider " + zSlider);



			// Z
			var z = index * 2 * 3 + 4;
			var newValueZ = zSlider;
			if (newValueZ > 9) {
				newValueZ = 9;
			}
			vertices = vertices.replaceAt(z, newValueZ);


			// Y
			var y = index * 2 * 3 + 2;
			var newValueY = ySlider;
			if (newValueY > 9) {
				newValueY = 9;
			}
			vertices = vertices.replaceAt(y, newValueY);


			// X
			var x = index * 2 * 3;
			var newValueX = xSlider;
			if (newValueX > 9) {
				newValueX = 9;
			}
			vertices = vertices.replaceAt(x, newValueX);

			var newPosition = newValueX + " " + newValueY + " " + newValueZ + " ";

			// Move the handler to new position


			transformURL = selectedElement.getAttribute('transform');
			transformURL = transformURL.replace('#', '');
			transform = document.getElementById(transformURL);



			console.log("transform " + transform);
			var translation = transform.setAttribute("translation", newPosition);
			console.log("translation " + translation);

			console.log("x:" + x);
			console.log("y:" + y);
			console.log("z:" + z);

			// console.log(vertices);

			$("#position").text(vertices);

			// console.log($("#position").text());

			// index = element.getAttribute("index");
			// console.log(index);
		}
	}
}

OG.click = function(element) {
	if (selectedElement != null) {
		selectedElement.setAttribute('shader', '#HandleShader');
	}
	element.setAttribute('shader', '#HandleShaderSelected');
	selectedElement = element;
	
	transformURL = element.getAttribute('transform');
	transformURL = transformURL.replace('#', '');
	transform = document.getElementById(transformURL);

	var translation = transform.getAttribute('translation');

	console.log("translation" + translation)

	var oldValueX = parseInt(translation.charAt(0));
	var oldValueY = parseInt(translation.charAt(2));
	var oldValueZ = parseInt(translation.charAt(4));
	console.log("oldValueX:" + oldValueX);
	console.log("oldValueY:" + oldValueY);
	console.log("oldValueZ:" + oldValueZ);


	$("#slider_weight1").slider({
		min: 0,
		max: 10,
		value: oldValueX,
		step: 1
	});

	$("#slider_weight2").slider({
		min: 0,
		max: 10,
		value: oldValueY,
		step: 1
	});

	$("#slider_weight3").slider({
		min: 0,
		max: 10,
		value: oldValueZ,
		step: 1
	});





}

OG.mouseOver = function(element, shaderName) {
	// console.log("element mouse over");
}

OG.mouseOut = function(element, shaderName) {
	// console.log("element mouse out");
	// setShader( element, shaderName, false );
}


// Texture parts of one object block
OG.vertex = {
	'_index': 0,
	'_alignment': OG.Alignments.LeftBottom,
	'vertex': function(index, alignment) {
		this._index = index;
		this._alignment = alignment;
	}
}

String.prototype.replaceAt = function(index, character) {
	return this.substr(0, index) + character + this.substr(index + 1, character.length);
}