    var anim = {};
    var vertices = ""; // string!
    var verticesLength = 0;
    var triangles = "";
    var normals = "";
    var uv = "";
    var selectedElement;

    function init() {
      var axisSize = 2;
      var objectSize = new OG.Vector3(axisSize, axisSize, axisSize);
      var map = new Array();

      // Create 3-dimensional array for the block map
      for (a = 0; a < axisSize; a++) {
        map[a] = new Array();
        for (b = 0; b < axisSize; b++) {
          map[a][b] = new Array();
        }
      }

      var x = 0;
      var y = 0;
      var z = 0;
      for (y = 0; y < objectSize.y; y++) {
        for (x = 0 + y; x < objectSize.x - y; x++) {
          for (z = 0 + y; z < objectSize.z - y; z++) {
            // if (axisSize == 1 || x == y  || y == z || z == x) {
            map[x][y][z] = true;

            // } else {
            //     map[x][y][z] = false;
            // }
          }
        }
      }

      // Blocks. Current room we are generating polygons to
      for (x = 0; x < objectSize.x; x++) {
        for (y = 0; y < objectSize.y; y++) {
          for (z = 0; z < objectSize.z; z++) {
            // Polygons. Make polygons for the block
            if (map[x][y][z]) {
              var blockPosition = new OG.Vector3(x, y, z);
              var orientation = OG.Orientations.zPlus; // Default orientation

              // Upper polygon (north)

              if (objectSize.z - 1 < z + 1 || !map[x][y][z + 1]) { // Make a upper polygon if there is no a block
                orientation = OG.Orientations.zPlus; // polygon goes from west to east
                createPolygon(orientation, blockPosition);
                createNormal(orientation);
                createUV();
              }
              // Right polygon (east)
              if (objectSize.x - 1 < x + 1 || !map[x + 1][y][z]) {
                orientation = OG.Orientations.xPlus; // north to south
                createPolygon(orientation, blockPosition);
                createNormal(orientation);
                createUV();
              }
              // Bottom polygon (south)
              if (0 > z - 1 || !map[x][y][z - 1]) {
                orientation = OG.Orientations.zMinus; // east to west;
                createPolygon(orientation, blockPosition);
                createNormal(orientation);
                createUV();
              }
              // Left polygon (west)
              if (0 > x - 1 || !map[x - 1][y][z]) {
                orientation = OG.Orientations.xMinus; // south to north
                createPolygon(orientation, blockPosition);
                createNormal(orientation);
                createUV();
              }
              // Roof
              if (objectSize.y - 1 < y + 1 || !map[x][y + 1][z]) {
                orientation = OG.Orientations.yPlus; // south to north
                createPolygon(orientation, blockPosition);
                createNormal(orientation);
                createUV();
              }
              // Floor
              if (0 > y - 1 || !map[x, y - 1, z]) {
                orientation = OG.Orientations.yMinus; // south to north;
                createPolygon(orientation, blockPosition);
                createNormal(orientation);
                createUV();
              }
            }
          }
        }
      }

      // console.log(vertices);
      // console.log(triangles);
      // console.log(normals);
      // console.log(uv);

      // Apply calculated mesh data to xml3d element
      $("#position").text(vertices);
      $("#triangles").text(triangles);
      $("#normals").text(normals);
      $("#uv").text(uv);

      // var floatti = document.getElementById("position");
      // var floatti = $("#position").text().replace(/ /g, '');
      // var floatti = $("#position").text();
      // console.log(floatti);

      // for (var i = 0; i < floatti.length; i++) {
      // console.log(floatti.charAt(5*2));
      // }

      // createVertexHandles();
    }

    function createUV() {
      uv = uv + "0.0  0.0 ";
      uv = uv + "1.0  0.0 ";
      uv = uv + "1.0  1.0 ";
      uv = uv + "0.0  1.0 ";
    }

    function createNormal(orientation) {
      switch (orientation) {
        case 0: // Z+
          // Front
          normals = normals + "1.0  0.0  1.0 ";
          normals = normals + "0.0  0.0  1.0 ";
          normals = normals + "0.0  0.0  1.0 ";
          normals = normals + "0.0  0.0  1.0 ";
          break;

        case 1: // Z-
          // Back
          normals = normals + "0.0  0.0 -1.0 ";
          normals = normals + "0.0  0.0 -1.0 ";
          normals = normals + "0.0  0.0 -1.0 ";
          normals = normals + "0.0  0.0 -1.0 ";
          break;

        case 2: // Y+
          // Top
          normals = normals + "0.0  1.0  0.0 ";
          normals = normals + "0.0  1.0  0.0 ";
          normals = normals + "0.0  1.0  0.0 ";
          normals = normals + "0.0  1.0  0.0 ";
          break;

        case 3: // Y-
          // Bottom
          normals = normals + "0.0 -1.0  0.0 ";
          normals = normals + "0.0 -1.0  0.0 ";
          normals = normals + "0.0 -1.0  0.0 ";
          normals = normals + "0.0 -1.0  0.0 ";
          break;

        case 4: // X-
          // Left
          normals = normals + "-1.0  0.0  0.0 ";
          normals = normals + "-1.0  0.0  0.0 ";
          normals = normals + "-1.0  0.0  0.0 ";
          normals = normals + "-1.0  0.0  0.0 ";
          break;

        case 5: // X+
          // Right
          normals = normals + "1.0  0.0  0.0 ";
          normals = normals + "1.0  0.0  0.0 ";
          normals = normals + "1.0  0.0  0.0 ";
          normals = normals + "1.0  0.0  0.0 ";
          break;
      }
    }

     // Create single polygon for the block

    function createPolygon(orientation, blockPosition) {
      // Vertices
      var leftBottom = createVertex(orientation, blockPosition, OG.Alignments.LeftBottom);
      var rightBottom = createVertex(orientation, blockPosition, OG.Alignments.RightBottom);
      var leftUpper = createVertex(orientation, blockPosition, OG.Alignments.LeftUpper);
      var rightUpper = createVertex(orientation, blockPosition, OG.Alignments.RightUpper);

      // Create bottom and upper triangles for the polygon
      createTriangles(leftBottom, rightBottom, leftUpper, rightUpper);
    }

    function createTriangles(bottomLeft, bottomRight, upperLeft, upperRight) {
      triangles = triangles + upperLeft + " " + bottomLeft + " " + upperRight + " " + bottomLeft + " " + bottomRight + " " + upperRight + " ";
    }

     // Determines the position of a vertex with block position and vertex's aligment on the block's polygon 

    function createVertex(orientation, blockPosition, alignment) {
      var vertexPosition = new OG.Vector3(blockPosition.x, blockPosition.y, blockPosition.z);

      // TexturePart tempTexturePart = new TexturePart (0);

      switch (orientation) {
        case 0: // Z+ , polygon is at z+ direction from the center of a block
          // Add polygon's position to vertexPosition
          vertexPosition.z += 1;

          // if (_blockTextureParts != null)
          //         tempTexturePart = _blockTextureParts [blockPosition].Zplus;

          //x
          if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
            vertexPosition.x++;
          }
          //y
          if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
            vertexPosition.y++;
          }

          break;
        case 1: // Z-
          // Add polygon's position to vertexPosition
          vertexPosition.x += 1;

          // if (_blockTextureParts != null)
          //         tempTexturePart = _blockTextureParts [blockPosition].Zminus;

          //X
          if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
            vertexPosition.x--;
          }
          //y
          if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
            vertexPosition.y++;
          }
          break;
        case 4: // X+
          // if (_blockTextureParts != null)
          //         tempTexturePart = _blockTextureParts [blockPosition].Xminus;

          //z
          if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
            vertexPosition.z++;
          }
          //y
          if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
            vertexPosition.y++;
          }

          break;
        case 5: // X-
          // Add polygon's position to vertexPosition
          vertexPosition.x += 1;
          vertexPosition.z += 1;

          // if (_blockTextureParts != null)
          //         tempTexturePart = _blockTextureParts [blockPosition].Xplus;

          //z
          if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
            vertexPosition.z--;
          }
          //y
          if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
            vertexPosition.y++;
          }
          break;
        case 2: // Roof
          vertexPosition.y += 1;

          // if (_blockTextureParts != null)
          //         tempTexturePart = _blockTextureParts [blockPosition].Yplus;

          //x
          if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
            vertexPosition.x++;
          }
          //z
          if (alignment == OG.Alignments.LeftBottom || alignment == OG.Alignments.RightBottom) {
            vertexPosition.z++;
          }
          break;
        case 3: // Floor
          // if (_blockTextureParts != null)
          //         tempTexturePart = _blockTextureParts [blockPosition].Yminus;

          //x
          if (alignment == OG.Alignments.RightBottom || alignment == OG.Alignments.RightUpper) {
            vertexPosition.x++;
          }
          //z
          if (alignment == OG.Alignments.LeftUpper || alignment == OG.Alignments.RightUpper) {
            vertexPosition.z++;
          }
          break;
      }

      // var hashSize = OG.Get(vertexHash);
      // var hashSize = Object.keys(vertexHash).length;
      // console.log("size: "+hashSize);
      // console.log(vertexPosition);
      var newVertex = OG.vertex.vertex(verticesLength, alignment);
      // console.log(vertexPosition);

      vertices = vertices + vertexPosition.x + " " + vertexPosition.y + " " + vertexPosition.z + " ";

      // // vertices.push(new OG.Vector3(vertexPosition.x, vertexPosition.y, vertexPosition.z));
      // vertices.push(new String(vertexPosition.x + ".0 "));
      // // vertices.push();
      // vertices.push(new String(vertexPosition.y + ".0 "));
      // // vertices.push(".0 ");
      // vertices.push(new String(vertexPosition.z + ".0 "));
      // // vertices.push(".0 ");

      createVertexHandle(vertexPosition);

      // return vertices.length - 1;
      verticesLength++;

      return verticesLength - 1;
    }

    function createVertexHandle(vertexPosition) {
      var transformId = "vertexHandle" + vertexPosition.x  + vertexPosition.y + vertexPosition.z;
      var groupId = "vertexHandleGroup" + vertexPosition.x + vertexPosition.y  + vertexPosition.z;

      var group = document.getElementById(groupId);
      if (group) {
        console.log("vertex handle already in the position! " + group);
        var vtxCount = group.getAttribute("vertexCount");
        console.log("vtxCount " + vtxCount);
        if (vtxCount == 1) {
          group.setAttribute("index2", verticesLength);
          group.setAttribute("vertexCount", 2); //TODO clean these

        } else if (vtxCount == 2) {
          group.setAttribute("index3", verticesLength);
          group.setAttribute("vertexCount", 3);

        } else if (vtxCount == 3) {
          group.setAttribute("index4", verticesLength);
          group.setAttribute("vertexCount", 4);

        }
      } else {
        console.log("creating a new vertex handle")
        var xml3d = document.getElementById("xml3d");
        var defs = document.getElementById("defs");
        var mesh = document.getElementById("cube");

        for (x = 0; x < 1; x++) {

          var transform = document.createElementNS("http://www.xml3d.org/2009/xml3d", "transform");
          var group = document.createElementNS("http://www.xml3d.org/2009/xml3d", "group");
          var mesh = document.createElementNS("http://www.xml3d.org/2009/xml3d", "mesh");


          // var posX = 3 * x;
          // var posZ = 3;
          // var posY = 3;
          // var translation = posX + " " + posY + " " + posZ;
          var translation = vertexPosition.x + " " + vertexPosition.y + " " + vertexPosition.z;
          // console.log("translation");
          group.setAttribute("id", groupId);
          transform.setAttribute("id", transformId);
          transform.setAttribute("translation", translation);
          transform.setAttribute("scale", "0.1 0.1 0.1");
          group.setAttribute("vertexCount", 1);

          mesh.setAttribute("src", "#cube");
          group.setAttribute("shader", "#HandleShader");
          // ID reference string that points to transform
          group.setAttribute("transform", "#" + transformId); 
          group.setAttribute("onmouseover", "OG.mouseOver(this,'#HandleShader')");
          group.setAttribute("onmouseout", "OG.mouseOut(this,'      #HandleShader    ')");
          group.setAttribute("onclick", "OG.click(this     )");
          // How many different vertices in this position?

          group.setAttribute("index1", verticesLength);


          defs.appendChild(transform);
          group.appendChild(mesh);
          xml3d.appendChild(group);

        }
      }
    }